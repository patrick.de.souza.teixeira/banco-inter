## Tecnologias 

- [JUnit5](https://junit.org/junit5/) - Plataforma para construção e execução de testes, de modo que o JUnit 5 é composto por diversos módulos com papéis diferentes (ao invés de “um único framework”):

- [H2 database](https://www.h2database.com/html/main.html) - Banco de dados em memória

- [Swagger](https://swagger.io/) - Trata-se de uma aplicação open source que auxilia desenvolvedores nos processos de definir, criar, documentar e consumir APIs REST.

- [Spring Boot](https://spring.io/projects/spring-boot) - O Spring Boot é uma ferramenta que visa facilitar o processo de configuração e publicação de aplicações que utilizem o ecossistema Spring..

- [Lombok](https://projectlombok.org/) - O Lombok é uma biblioteca Java focada em produtividade e redução de código boilerplate que, por meio de anotações adicionadas ao nosso código, ensinamos o compilador (maven ou gradle) durante o processo de compilação a criar código Java.


## Pré requisitos

 - [Maven](https://maven.apache.org/) - Ferramenta de automação de compilação utilizada primariamente em projetos Java.
 - [Docker](https://docs.docker.com/get-docker/) - Tecnologia que permite aos desenvolvedores empacotar, entregar e executar aplicações em containers Linux leves e autossuficientes.


## Definição do Projeto

Dado um inteiro, precisamos encontrar o dígito único do inteiro.

Se '{digit}' tem apenas um dígito, então o seu dígito único é '{unique-digit}'.
Caso contrário, o dígito único de '{digit}' é igual ao dígito único da soma dos dígitos de '{digit}'.

Por exemplo, o dígito único de 9875 será calculado como:

'{digito_unico(9875)9+8+7+5=29}'
'{digito_unico(29)2+9=11}'

Dado dois números '{digit}' e '{multiplier}', deverá ser criado da concatenação da string '{digit}' * '{multiplier}'.

Exemplo:

'{digit}'=9875 e '{digit}'=4 então o valor concatenado será = '{9875987598759875}'
'{unique-digit}' = 5+7+8+9+5+7+8+9+5+7+8+9+5+7+8+9=116
'{unique-digit}' = 1+1+6=8
'{unique-digit}' = 8

## Execução do Projeto via Docker

 - Realize o clone o projeto ' $ git clone https://gitlab.com/patrick.de.souza.teixeira/banco-inter.git' e acesse o diretório do projeto 'cd banco-inter'.
 - Basta executar o script '{docker-run.sh'} dando permissão de execução para o arquivo '{$ chmod +x docker-run.sh'}
 - Para testar se o projeto está executando, acesse a documentação da API do projeto [clicando aqui](http://localhost:8082/swagger-inter.html)

## Execução do Projeto via Maven

- Executar o comando mvn spring-boot:run que será executado o projeto ou mvn install para gerar um .jar que ficara disponível na pasta target, sendo assim pode-se executar o comando java -jar target/banco-inter-0.0.1-SNAPSHOT.jar
- Ou direto de sua ide
- Caso queira rodar os testes basta dar o comando mvn test

## Documentação do Projeto 

 - [Swagger](http://localhost:8080/swagger-inter.html) - Documentação de chamadas.
 - [Swagger](http://localhost:8082/swagger-inter.html) - Documentação de chamadas via Docker.
 - [Api-Docs](http://localhost:8080/v3/api-docs) - Documentação de chamadas em Json.
 - [Api-Docs](http://localhost:8082/v3/api-docs) - Documentação de chamadas em Json via Docker.  
 
 