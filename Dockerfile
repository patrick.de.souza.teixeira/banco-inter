FROM openjdk

WORKDIR /app

COPY target/banco-inter-1.0-SNAPSHOT.jar /app/banco-inter.jar

ENTRYPOINT ["java", "-jar", "banco-inter.jar"] 