package br.com.inter.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.inter.domain.dto.UserApplicationDTO;
import br.com.inter.domain.dto.request.UserApplicationDTORequest;
import br.com.inter.util.TesteHelper;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserApplicationControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	private TesteHelper testeHelper = new TesteHelper();
	private String urlDefaultUserApplication = "http://localhost:8080/inter/userApplication/";

//	@Test
//	@Order(1)
//	@DisplayName("Cria usuário com todos os valores válidos.")
//	public void CreateUserWithAllInformationValidateShouldReturn201StatusCode() throws Exception {
//		UserApplicationDTORequest user = testeHelper.userWithAllInformation();
//		mockMvc.perform(post(urlDefaultUserApplication + "addUser").contentType("application/json")
//				.content(objectMapper.writeValueAsString(user))).andExpect(status().isCreated());
//	}

	@Test
	@Order(2)
	@DisplayName("Tenta criar usuário com e-mail e nome fora dos padrões.")
	public void CreateUserWithNameAndEmailNonStandardShouldReturn404StatusCode() throws Exception {
		UserApplicationDTORequest user = testeHelper.userWithNameAndEmailIncomplete();
		mockMvc.perform(post(urlDefaultUserApplication + "addUser").contentType("application/json")
				.content(objectMapper.writeValueAsString(user))).andExpect(status().isBadRequest());
	}

	@Test
	@Order(3)
	@DisplayName("Busca usuário por código.")
	public void GetUserShouldReturn200StatusCode() throws Exception {
		mockMvc.perform(get(urlDefaultUserApplication + "findbyID/{id}", 1).contentType("application/json")
				.content(objectMapper.writeValueAsString(1))).andExpect(status().isOk());

	}

	@Test
	@Order(4)
	@DisplayName("Busca usuário por código inexistente.")
	public void GetUserShouldReturn404StatusCode() throws Exception {
		mockMvc.perform(get(urlDefaultUserApplication + "findbyID/{id}", 999).contentType("application/json")
				.content(objectMapper.writeValueAsString(1))).andExpect(status().isNotFound());

	}

	@Test
	@Order(5)
	@DisplayName("Altera todos os dados do usuário existente na base de dados.")
	public void UpdateAllInformationOftheUserShouldReturn200StatusCode() throws Exception {
		UserApplicationDTO user = testeHelper.userWithAllInformationChanged();
		mockMvc.perform(put(urlDefaultUserApplication + "updateUser").contentType("application/json")
				.content(objectMapper.writeValueAsString(user))).andExpect(status().isOk());

	}

	@Test
	@Order(6)
	@DisplayName("Busca todos os usuário cadastrados na base.")
	public void GetAllUserShouldReturn200StatusCode() throws Exception {
		mockMvc.perform(get(urlDefaultUserApplication + "findAllUsers").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	@Order(7)
	@DisplayName("Busca todos os dígitos únicos cadastrados na base.")
	public void GetAllDigitsShouldReturn200StatusCode() throws Exception {
		mockMvc.perform(get(urlDefaultUserApplication + "findAllDigit").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	@Order(8)
	@DisplayName("Delete Usuário Cadastrado na base por ID.")
	public void DeleteExistingUserInTheDatabaseShouldReturn204StatusCode() throws Exception {
		mockMvc.perform(delete(urlDefaultUserApplication + "deleteUserById/{id}", 1).contentType("application/json")
				.content(objectMapper.writeValueAsString(1))).andExpect(status().isNoContent());

	}

	@Test
	@Order(9)
	@DisplayName("Delete usuário inexistente na base por ID.")
	public void DeleteNonExistentUserInTheDatabaseShouldReturn404StatusCode() throws Exception {
		mockMvc.perform(delete(urlDefaultUserApplication + "deleteUserById/{id}", 999).contentType("application/json")
				.content(objectMapper.writeValueAsString(1))).andExpect(status().isNotFound());

	}
}
