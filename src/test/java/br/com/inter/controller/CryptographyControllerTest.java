package br.com.inter.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.inter.domain.dto.CryptographyDTO;
import br.com.inter.domain.dto.request.UserApplicationDTORequest;
import br.com.inter.util.TesteHelper;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CryptographyControllerTest {

	@Autowired
	private MockMvc mockMvc;
	@Autowired
	private ObjectMapper objectMapper;

	private TesteHelper testeHelper = new TesteHelper();
	private String urlDefaultCryptography = "http://localhost:8080/inter/cryptography/";
	private String urlDefaultUserApplication = "http://localhost:8080/inter/userApplication/";

	@Test
	@Order(1)
	@DisplayName("Cria usuário com todos os valores válidos para gerar chave de criptografia.")
	public void CreateUserWithAllInformationValidateShouldReturn201StatusCode() throws Exception {
		UserApplicationDTORequest user = testeHelper.userWithAllInformation();
		mockMvc.perform(post(urlDefaultUserApplication + "addUser").contentType("application/json")
				.content(objectMapper.writeValueAsString(user))).andExpect(status().isCreated());
	}

	@Test
	@Order(2)
	@DisplayName("Gera chaves de criptografia RSA para usuário existente.")
	public void CreateKeyRSAtoUserShouldReturn204StatusCode() throws Exception {
		CryptographyDTO key = testeHelper.existingUserToEncrypt();
		mockMvc.perform(post(urlDefaultCryptography + "generatesEncryptionKey").contentType("application/json")
				.content(objectMapper.writeValueAsString(key))).andExpect(status().isNoContent());

	}

	@Test
	@Order(3)
	@DisplayName("Gera chaves de criptografia RSA para usuário inexistente.")
	public void CreateKeyRSAtoUserNonExistentShouldReturn404StatusCode() throws Exception {
		CryptographyDTO key = testeHelper.nonExistingUserToEncrypt();
		mockMvc.perform(post(urlDefaultCryptography + "generatesEncryptionKey").contentType("application/json")
				.content(objectMapper.writeValueAsString(key))).andExpect(status().isNotFound());

	}

}
