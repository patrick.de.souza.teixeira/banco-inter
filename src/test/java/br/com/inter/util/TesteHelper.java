package br.com.inter.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.inter.domain.dto.CryptographyDTO;
import br.com.inter.domain.dto.UserApplicationDTO;
import br.com.inter.domain.dto.request.UniqueDigitDTORequest;
import br.com.inter.domain.dto.request.UserApplicationDTORequest;
import br.com.inter.domain.model.UniqueDigitModel;

public class TesteHelper {

	public UserApplicationDTORequest userWithAllInformation() {
		UserApplicationDTORequest user = new UserApplicationDTORequest();
		user.setNameUser("Darth Vader Da Silva");
		user.setEmailUser("darth@vader.com");
		List<UniqueDigitDTORequest> listUniqueDigit = new ArrayList<>();
		Random rn = new Random();
		for (int i = 0; i < 5; i++) {
			UniqueDigitDTORequest uniqueDigitDTORequest = new UniqueDigitDTORequest();
			uniqueDigitDTORequest.setDigit(String.valueOf(rn.nextInt(10) + 1));
			uniqueDigitDTORequest.setMultiplier(rn.nextInt(10) + 1);
			listUniqueDigit.add(uniqueDigitDTORequest);
		}
		user.setListUniqueDigit(listUniqueDigit);
		return user;
	}

	public UserApplicationDTORequest userWithNameAndEmailIncomplete() {
		UserApplicationDTORequest user = new UserApplicationDTORequest();
		user.setNameUser("Da");
		user.setEmailUser("darth.vader.com");
		List<UniqueDigitDTORequest> listUniqueDigit = new ArrayList<>();
		Random rn = new Random();
		for (int i = 0; i < 5; i++) {
			UniqueDigitDTORequest uniqueDigitDTORequest = new UniqueDigitDTORequest();
			uniqueDigitDTORequest.setDigit(String.valueOf(rn.nextInt(10) + 1));
			uniqueDigitDTORequest.setMultiplier(rn.nextInt(10) + 1);
			listUniqueDigit.add(uniqueDigitDTORequest);
		}
		user.setListUniqueDigit(listUniqueDigit);
		return user;
	}

	public UserApplicationDTO userWithAllInformationChanged() {
		UserApplicationDTO user = new UserApplicationDTO();
		user.setId(1L);
		user.setNameUser("Anakin Skywalker");
		user.setEmailUser("Anakin@Skywalker.com");
		List<UniqueDigitModel> listUniqueDigit = new ArrayList<>();
		Random rn = new Random();
		for (Long i = 1L; i < 6; i++) {
			UniqueDigitModel uniqueDigitModel = new UniqueDigitModel();
			uniqueDigitModel.setId(i);
			uniqueDigitModel.setDigit(String.valueOf(rn.nextInt(10) + 1));
			uniqueDigitModel.setMultiplier(rn.nextInt(10) + 1);
			listUniqueDigit.add(uniqueDigitModel);
		}
		user.setListUniqueDigit(listUniqueDigit);
		return user;
	}

	public CryptographyDTO existingUserToEncrypt() {
		CryptographyDTO cryptographyDTO = new CryptographyDTO();
		cryptographyDTO.setIdUser(1L);
		return cryptographyDTO;
	}

	public CryptographyDTO nonExistingUserToEncrypt() {
		CryptographyDTO cryptographyDTO = new CryptographyDTO();
		cryptographyDTO.setIdUser(999L);
		return cryptographyDTO;
	}

}
