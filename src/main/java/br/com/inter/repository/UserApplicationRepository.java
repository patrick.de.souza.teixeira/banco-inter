package br.com.inter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.inter.domain.model.UserApplicationModel;

public interface UserApplicationRepository extends JpaRepository<UserApplicationModel, Long> {

	Optional<UserApplicationModel> findByNameUserAndEmailUser(String nameUser, String emailUser);

}
