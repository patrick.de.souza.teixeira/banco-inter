package br.com.inter.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.inter.domain.model.UniqueDigitModel;

@Repository
public interface UniqueDigitRepository extends JpaRepository<UniqueDigitModel, Long> {

}
