package br.com.inter.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.inter.domain.model.CryptographyModel;

@Repository
public interface CryptographyRepository extends JpaRepository<CryptographyModel, Long> {
	Optional<CryptographyModel> findByIdUser(Long idUser);
}
