package br.com.inter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Desafio Banco Inter - Dígito Único.", version = "1.0", description = "Desenvolvimento de API para cálculo de dígito único do processo seletivo.", license = @License(name = "SpringDocs", url = "https://springdoc.org"), contact = @Contact(url = "https://www.linkedin.com/in/patrick-de-souza-teixeira/", name = "Patrick Teixeira", email = "patrick.de.souza.teixeira@gmail.com")))

public class InterApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(InterApiApplication.class, args);
	}

}
