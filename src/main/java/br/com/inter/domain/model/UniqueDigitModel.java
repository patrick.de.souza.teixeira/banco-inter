package br.com.inter.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "digit")
public class UniqueDigitModel {

	public UniqueDigitModel(String digit, Integer multiplier) {
	}

	public UniqueDigitModel(Long id, String digit, Integer multiplier) {

	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String digit;
	private Integer multiplier;
	private Integer uniqueDigit;
	@JsonIgnore
	private Long idUser;

}
