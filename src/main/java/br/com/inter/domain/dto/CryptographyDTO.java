package br.com.inter.domain.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.validation.executable.ValidateOnExecution;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.inter.domain.model.CryptographyModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ValidateOnExecution
public class CryptographyDTO {

	@NotNull(message = "Por favor informe o ID do usuário para gerar as chaves de criptografia")
	private Long idUser;
	@JsonIgnore
	@Size(min = 1, max = 3000)
	private String privateKey;
	@JsonIgnore
	@Size(min = 1, max = 3000)
	private String publicKey;

	public CryptographyModel build() {
		return new CryptographyModel().setIdUser(this.idUser).setPrivateKey(this.privateKey)
				.setPublicKey(this.publicKey);
	}
}
