package br.com.inter.domain.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.executable.ValidateOnExecution;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.inter.domain.model.UniqueDigitModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
@ValidateOnExecution
public class UniqueDigitDTORequest {

	@NotBlank
	@Size(min = 1, max = 100000)
	@Pattern(regexp = "[0-9]*", message = "O campo deve conter apenas números inteiros positívos.")
	private String digit;

	@Min(1)
	@NotNull(message = "{invalid.multiplier}")
	private Integer multiplier;

	@JsonIgnore
	private Integer uniqueDigit;
	@JsonIgnore
	private Long idUser;

	public UniqueDigitModel build() {
		return new UniqueDigitModel().setDigit(this.digit).setMultiplier(this.multiplier).setUniqueDigit(uniqueDigit)
				.setIdUser(idUser);

	}
}