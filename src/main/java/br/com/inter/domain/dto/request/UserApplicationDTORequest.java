package br.com.inter.domain.dto.request;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import javax.validation.executable.ValidateOnExecution;

import br.com.inter.domain.model.UniqueDigitModel;
import br.com.inter.domain.model.UserApplicationModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ValidateOnExecution
public class UserApplicationDTORequest {

	@NotBlank
	@Size(min = 3, max = 50)
	private String nameUser;

	@NotBlank
	@Email
	@Size(min = 10, max = 50)
	private String emailUser;

	@NotEmpty
	private List<UniqueDigitDTORequest> listUniqueDigit;

	public UserApplicationModel build() {

		UserApplicationModel userApplicationModel = new UserApplicationModel();
		userApplicationModel.setNameUser(this.nameUser);
		userApplicationModel.setEmailUser(emailUser);
		userApplicationModel.setListUniqueDigit(listUniqueDigit.stream()
				.map(p -> new UniqueDigitModel(p.getDigit(), p.getMultiplier())).collect(Collectors.toList()));
		return userApplicationModel;

	}
}
