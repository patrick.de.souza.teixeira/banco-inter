package br.com.inter.domain.dto.response;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;
import javax.validation.executable.ValidateOnExecution;

import br.com.inter.domain.model.UniqueDigitModel;
import br.com.inter.domain.model.UserApplicationModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ValidateOnExecution
public class UserApplicationDTOResponse {

	private long id;
	@NotBlank
	private String nameUser;
	private String emailUser;
	private List<UniqueDigitDTOResponse> listUniqueDigit;

	public UserApplicationModel build() {

		UserApplicationModel userApplicationModel = new UserApplicationModel();
		userApplicationModel.setId(this.id);
		userApplicationModel.setNameUser(this.nameUser);
		userApplicationModel.setEmailUser(emailUser);
		userApplicationModel.setListUniqueDigit(
				listUniqueDigit.stream().map(p -> new UniqueDigitModel(p.getId(), p.getDigit(), p.getMultiplier()))
						.collect(Collectors.toList()));
		return userApplicationModel;

	}

}
