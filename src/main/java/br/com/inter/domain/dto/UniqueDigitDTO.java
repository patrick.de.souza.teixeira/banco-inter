package br.com.inter.domain.dto;

import br.com.inter.domain.model.UniqueDigitModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UniqueDigitDTO {

	private Long id;
	private String digit;
	private Integer multiplier;
	private Integer uniqueDigit;
	private Long idUser;

	public UniqueDigitModel build() {
		return new UniqueDigitModel().setId(this.id).setDigit(this.digit).setMultiplier(this.multiplier)
				.setUniqueDigit(this.uniqueDigit).setIdUser(this.idUser);
	}
}
