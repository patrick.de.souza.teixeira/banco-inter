package br.com.inter.domain.dto;

import java.util.List;

import javax.validation.executable.ValidateOnExecution;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import br.com.inter.domain.model.UniqueDigitModel;
import br.com.inter.domain.model.UserApplicationModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties
@ValidateOnExecution
public class UserApplicationDTO {

	@JsonProperty(access = JsonProperty.Access.READ_ONLY)
	private Long id;
	private String nameUser;
	private String emailUser;
	private List<UniqueDigitModel> listUniqueDigit;

	public UserApplicationModel build() {
		return new UserApplicationModel().setNameUser(this.nameUser).setEmailUser(this.emailUser)
				.setListUniqueDigit(this.listUniqueDigit);

	}

}
