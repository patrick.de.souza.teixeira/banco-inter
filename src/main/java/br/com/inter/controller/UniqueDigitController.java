package br.com.inter.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.domain.dto.request.UniqueDigitDTORequest;
import br.com.inter.domain.dto.response.UniqueDigitDTOResponse;
import br.com.inter.service.UniqueDigitService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/inter/uniqueDigit/")

public class UniqueDigitController {

	@Autowired
	private UniqueDigitService uniqueDigitService;

	@Operation(operationId = "calcUniqueDigit", summary = "Calcula dígito dos valores informados.", responses = {
			@ApiResponse(responseCode = "200", description = "Dígito único calculado com sucesso."),
			@ApiResponse(responseCode = "400", description = "Json fora da padronização."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PostMapping(path = "calcUniqueDigit", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public UniqueDigitDTOResponse calcUniqueDigit(@Valid @RequestBody UniqueDigitDTORequest uniqueDigitDTORequest) {
		return uniqueDigitService.calcUniqueDigit(uniqueDigitDTORequest);
	}
}
