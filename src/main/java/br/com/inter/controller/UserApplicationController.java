package br.com.inter.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.domain.dto.UserApplicationDTO;
import br.com.inter.domain.dto.request.UserApplicationDTORequest;
import br.com.inter.domain.dto.response.UserApplicationDTOResponse;
import br.com.inter.domain.model.UniqueDigitModel;
import br.com.inter.domain.model.UserApplicationModel;
import br.com.inter.service.UserApplicationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/inter/userApplication/")
public class UserApplicationController {

	@Autowired
	private UserApplicationService userApplicationService;

	@Operation(operationId = "addUser", summary = "Cadastro de usuário juntamente com a lista de valores para o cálculo do dígito único.", responses = {
			@ApiResponse(responseCode = "201", description = "Usuário criado com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PostMapping(path = "addUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserApplicationDTO> newUser(
			@Valid @RequestBody UserApplicationDTORequest userApplicationDTORequest) {
		return userApplicationService.newUser(userApplicationDTORequest);
	}

	@Operation(operationId = "updateUser", summary = "Edição de usuário e lista de valores para o cálculo do dígito único.", responses = {
			@ApiResponse(responseCode = "202", description = "Usuário editado com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PutMapping(path = "updateUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity<UserApplicationDTO> updateUser(
			@RequestBody UserApplicationDTOResponse userApplicationDTOResponse) {
		return userApplicationService.updateUser(userApplicationDTOResponse);

	}

	@Operation(operationId = "deleteUserById", summary = "Exclusão de usuário juntamente com sua lista de valores de dígito único.", responses = {
			@ApiResponse(responseCode = "204", description = "Solicitação foi bem sucedida. Usuário excluido com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@DeleteMapping("deleteUserById/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<Void> deleteUser(@Valid @PathVariable Long id) {
		return userApplicationService.deleteUser(id);
	}

	@Operation(operationId = "findbyID", summary = "Busca de usuário por identificador.", responses = {
			@ApiResponse(responseCode = "200", description = "Usuário localizado com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@GetMapping("findbyID/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<UserApplicationModel> findbyID(@PathVariable Long id) {
		return userApplicationService.findbyID(id);
	}

	@Operation(operationId = "findAllDigit", summary = "Busca de todos os digitos cadastrados.", responses = {
			@ApiResponse(responseCode = "200", description = "Digitos localizados com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@GetMapping("findAllDigit")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<UniqueDigitModel>> findAllUniqueDigit() {
		return userApplicationService.findAllUniqueDigit();
	}

	@Operation(operationId = "findAllUsers", summary = "Busca de todos os usuários cadastrados.", responses = {
			@ApiResponse(responseCode = "200", description = "Usuários localizados com sucesso."),
			@ApiResponse(responseCode = "401", description = "O usuário deve se autenticar para obter a resposta solicitada."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@GetMapping("findAllUsers")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<List<UserApplicationModel>> findAllUser() {
		return userApplicationService.findAllUser();
	}

}
