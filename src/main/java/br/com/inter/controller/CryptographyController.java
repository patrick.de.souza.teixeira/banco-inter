package br.com.inter.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.inter.domain.dto.CryptographyDTO;
import br.com.inter.service.CryptographyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping("/inter/cryptography/")
public class CryptographyController {

	@Autowired
	private CryptographyService cryptographyService;

	@Operation(operationId = "generatesEncryptionKey", summary = "Gera chave de criptografia por usuário.", responses = {
			@ApiResponse(responseCode = "204", description = "Chave de criptografia gerada com sucesso."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PostMapping(path = "generatesEncryptionKey", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> generatesEncryptionKey(@Valid @RequestBody CryptographyDTO cryptographyDTO) {
		return cryptographyService.generateKey(cryptographyDTO.getIdUser());
	}

	@Operation(operationId = "encryptUser", summary = "Criptografa dados do usuário.", responses = {
			@ApiResponse(responseCode = "204", description = "Dados criptografados com sucesso."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PostMapping(path = "encryptUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> encryptUser(@Valid @RequestBody CryptographyDTO cryptographyDTO) {
		return cryptographyService.encryptUser(cryptographyDTO.getIdUser());
	}

	@Operation(operationId = "decryptUser", summary = "Descriptografa dados do usuário.", responses = {
			@ApiResponse(responseCode = "204", description = "Dados descriptografados com sucesso."),
			@ApiResponse(responseCode = "404", description = "O servidor não pode encontrar o recurso solicitado."),
			@ApiResponse(responseCode = "500", description = "O servidor encontrou uma situação com a qual não sabe lidar.") })
	@PostMapping(path = "decryptUser", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> decryptUser(@Valid @RequestBody CryptographyDTO cryptographyDTO) {
		return cryptographyService.decryptUser(cryptographyDTO.getIdUser());

	}

}
