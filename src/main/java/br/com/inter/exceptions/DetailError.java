package br.com.inter.exceptions;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class DetailError {
	private Integer status;
	private LocalDateTime dateTime;
	private String messageError;
	private List<DetailFieldError>  detailError;
	
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	public static class DetailFieldError{
		private String fieldName;
		private String messageFieldError;
	}
}
