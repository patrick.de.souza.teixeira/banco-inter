package br.com.inter.exceptions;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(ApiException.class)
	private ResponseEntity<Object> handleApi(ApiException ex, WebRequest request) {
		var problem = fillsProblem(HttpStatus.BAD_REQUEST, ex.getMessage());
		return handleExceptionInternal(ex, problem, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		var detailFieldsErros = new ArrayList<DetailError.DetailFieldError>();

		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			String field = ((FieldError) error).getField();
			String message = messageSource.getMessage(error, LocaleContextHolder.getLocale());
			detailFieldsErros.add(new DetailError.DetailFieldError(field, message));
		}

		var problem = fillsProblem(status, detailFieldsErros,
				"Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente.");

		return super.handleExceptionInternal(ex, problem, headers, status, request);
	}

	private DetailError fillsProblem(HttpStatus status, ArrayList<DetailError.DetailFieldError> detailFieldsErros,
			String messageErro) {
		var problem = new DetailError();
		problem.setStatus(status.value());
		problem.setDetailError(detailFieldsErros);
		problem.setMessageError(messageErro);
		problem.setDateTime(LocalDateTime.now());
		return problem;
	}

	private DetailError fillsProblem(HttpStatus status, String messageErro) {
		var problem = new DetailError();
		problem.setStatus(status.value());
		problem.setMessageError(messageErro);
		problem.setDateTime(LocalDateTime.now());
		return problem;
	}

}
