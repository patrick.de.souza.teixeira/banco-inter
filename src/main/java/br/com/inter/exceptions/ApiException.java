package br.com.inter.exceptions;

public class ApiException extends RuntimeException {

	private static final long serialVersionUID = 4966866775551958572L;

	public ApiException (String message) {
		super(message);
	}

}
