package br.com.inter.service;

import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.inter.domain.dto.request.UniqueDigitDTORequest;
import br.com.inter.domain.dto.response.UniqueDigitDTOResponse;

@Service
public class CacheService {

	@Autowired
	private UniqueDigitService uniqueDigitService;

	private static TreeMap<String, Integer> cacheDigit = new TreeMap<>();

	private static void addCache(String keyMap, Integer valueMap) {
		boolean full = verifySizeCache();
		if (full) {
			cacheDigit.pollFirstEntry();
		}
		cacheDigit.put(keyMap, valueMap);
	}

	private static boolean verifySizeCache() {
		return (cacheDigit.size() == 10);
	}

	public Integer addIfNotFoundInCache(String digit, Integer multiplier) {
		String keyMap = digit + multiplier.toString();

		if (cacheDigit.containsKey(keyMap)) {
			return cacheDigit.get(keyMap);
		}
		UniqueDigitDTORequest uniqueDigitReq = new UniqueDigitDTORequest();
		uniqueDigitReq.setDigit(digit);
		uniqueDigitReq.setMultiplier(multiplier);
		UniqueDigitDTOResponse uniqueDigitResp = uniqueDigitService.calcUniqueDigit(uniqueDigitReq);
		addCache(keyMap, uniqueDigitResp.getUniqueDigit());
		return uniqueDigitResp.getUniqueDigit();
	}
}
