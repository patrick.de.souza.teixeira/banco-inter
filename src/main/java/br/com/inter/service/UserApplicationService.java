package br.com.inter.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.inter.domain.dto.UserApplicationDTO;
import br.com.inter.domain.dto.request.UserApplicationDTORequest;
import br.com.inter.domain.dto.response.UserApplicationDTOResponse;
import br.com.inter.domain.model.UniqueDigitModel;
import br.com.inter.domain.model.UserApplicationModel;
import br.com.inter.exceptions.ApiException;
import br.com.inter.repository.UniqueDigitRepository;
import br.com.inter.repository.UserApplicationRepository;

@Service
public class UserApplicationService {

	@Autowired
	private UserApplicationRepository repositoryUser;

	@Autowired
	private UniqueDigitRepository repositoryDigit;

	@Autowired
	private CacheService cacheService;

	public ResponseEntity<UserApplicationDTO> newUser(UserApplicationDTORequest userApplicationDTORequest) {

		if (userApplicationDTORequest.getListUniqueDigit().isEmpty()) {
			throw new ApiException("Lista de parâmetros para o cálculo de dígito único vazia.");
		}

		Optional<UserApplicationModel> userExist = repositoryUser.findByNameUserAndEmailUser(
				userApplicationDTORequest.getNameUser(), userApplicationDTORequest.getEmailUser());
		var newUser = new UserApplicationModel().setEmailUser(userApplicationDTORequest.getEmailUser())
				.setNameUser(userApplicationDTORequest.getNameUser());

		if (!userExist.isPresent()) {
			newUser = repositoryUser.save(newUser);
		} else {
			throw new ApiException("Usuário já existente na base de dados. Escolha a opção de editar.");
		}

		final Long codNewUser = newUser.getId();

		List<UniqueDigitModel> listUniqueDigitByUser = new ArrayList<>();
		userApplicationDTORequest.getListUniqueDigit().forEach(item -> {
			item.setIdUser(codNewUser);
			item.setUniqueDigit(cacheService.addIfNotFoundInCache(item.getDigit(), item.getMultiplier()));
			listUniqueDigitByUser.add(repositoryDigit.save(item.build()));
		});

		return ResponseEntity.created(URI.create(""))
				.body(fillsReturnTypeUserApplicationDTO(codNewUser, userApplicationDTORequest.getNameUser(),
						userApplicationDTORequest.getEmailUser(), listUniqueDigitByUser));
	}

	public ResponseEntity<UserApplicationDTO> updateUser(UserApplicationDTOResponse userApplicationDTOResponse) {

		if (userApplicationDTOResponse.getListUniqueDigit().isEmpty()) {
			throw new ApiException("Lista de parâmetros para o cálculo de dígito único vazia.");
		}

		if (repositoryUser.findById(userApplicationDTOResponse.getId()).isPresent()) {

			var updatedUser = new UserApplicationModel().setId(userApplicationDTOResponse.getId())
					.setEmailUser(userApplicationDTOResponse.getEmailUser())
					.setNameUser(userApplicationDTOResponse.getNameUser());

			repositoryUser.save(updatedUser);

			List<UniqueDigitModel> listUniqueDigitByUser = new ArrayList<>();

			userApplicationDTOResponse.getListUniqueDigit().forEach(item -> {
				item.setId(item.getId());
				item.setDigit(item.getDigit());
				item.setMultiplier(item.getMultiplier());
				item.setUniqueDigit(cacheService.addIfNotFoundInCache(item.getDigit(), item.getMultiplier()));
				item.setIdUser(updatedUser.getId());
				listUniqueDigitByUser.add(repositoryDigit.save(item.build()));
			});

			return ResponseEntity.ok(fillsReturnTypeUserApplicationDTO(userApplicationDTOResponse.getId(),
					userApplicationDTOResponse.getNameUser(), userApplicationDTOResponse.getEmailUser(),
					listUniqueDigitByUser));

		}
		return ResponseEntity.notFound().build();
	}

	public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
		if (!repositoryUser.existsById(id)) {
			return ResponseEntity.notFound().build();
		}
		repositoryUser.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	public ResponseEntity<UserApplicationModel> findbyID(Long id) {
		Optional<UserApplicationModel> userApplicationExist = repositoryUser.findById(id);
		if (userApplicationExist.isPresent()) {
			return ResponseEntity.ok(userApplicationExist.get());
		}
		return ResponseEntity.notFound().build();
	}

	public ResponseEntity<List<UniqueDigitModel>> findAllUniqueDigit() {
		return ResponseEntity.ok(repositoryDigit.findAll());
	}

	public ResponseEntity<List<UserApplicationModel>> findAllUser() {
		return ResponseEntity.ok(repositoryUser.findAll());
	}

	private UserApplicationDTO fillsReturnTypeUserApplicationDTO(Long codUser, String nameUser, String emailUser,
			List<UniqueDigitModel> listUniqueDigitByUser) {
		UserApplicationDTO userApplicationDTO = new UserApplicationDTO();
		userApplicationDTO.setId(codUser);
		userApplicationDTO.setEmailUser(emailUser);
		userApplicationDTO.setNameUser(nameUser);
		userApplicationDTO.setListUniqueDigit(listUniqueDigitByUser);
		return userApplicationDTO;
	}

	public UserApplicationModel getRegisteredUser(Long idUser) {
		UserApplicationModel user = repositoryUser.findById(idUser)
				.orElseThrow(() -> new ApiException("Não existe usuário com este código de identificação."));
		return user;
	}

}
