package br.com.inter.service;

import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import javax.crypto.Cipher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.inter.domain.dto.CryptographyDTO;
import br.com.inter.domain.model.CryptographyModel;
import br.com.inter.domain.model.UserApplicationModel;
import br.com.inter.exceptions.ApiException;
import br.com.inter.repository.CryptographyRepository;
import br.com.inter.repository.UserApplicationRepository;

@Service
public class CryptographyService {

	private static final String METHOD = "RSA";
	private static final Integer MODULE = 2048;

	@Autowired
	private CryptographyRepository repositoryCrypt;

	@Autowired
	private UserApplicationService userApplicationService;

	@Autowired
	private UserApplicationRepository repositoryUser;

	public ResponseEntity<Void> generateKey(Long id) {

		if (!repositoryUser.findById(id).isPresent()) {
			return ResponseEntity.notFound().build();
		}

		try {

			KeyPairGenerator keyGen = KeyPairGenerator.getInstance(METHOD);
			keyGen.initialize(MODULE);
			KeyPair keyPair = keyGen.generateKeyPair();
			CryptographyDTO cryptographyDTO = new CryptographyDTO();
			cryptographyDTO.setIdUser(id);
			cryptographyDTO.setPrivateKey(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()));
			cryptographyDTO.setPublicKey(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()));
			repositoryCrypt.save(cryptographyDTO.build());
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			throw new ApiException("Erro na geração de Chaves." + e.getMessage());
		}
	}

	public ResponseEntity<Void> encryptUser(Long idUser) {
		if (!repositoryCrypt.findByIdUser(idUser).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		CryptographyModel cryptographyModel = getCryptografyByUser(idUser);
		UserApplicationModel user = userApplicationService.getRegisteredUser(idUser);
		user.setId(user.getId());
		user.setEmailUser(
				Base64.getEncoder().encodeToString(encrypt(user.getEmailUser(), cryptographyModel.getPublicKey())));
		user.setNameUser(
				Base64.getEncoder().encodeToString(encrypt(user.getNameUser(), cryptographyModel.getPublicKey())));
		repositoryUser.save(user);
		return ResponseEntity.noContent().build();
	}

	public ResponseEntity<Void> decryptUser(Long idUser) {
		if (!repositoryCrypt.findByIdUser(idUser).isPresent()) {
			return ResponseEntity.notFound().build();
		}
		CryptographyModel cryptographyModel = getCryptografyByUser(idUser);
		UserApplicationModel user = userApplicationService.getRegisteredUser(idUser);
		user.setId(user.getId());
		user.setEmailUser(decrypt(Base64.getDecoder().decode(user.getEmailUser()), cryptographyModel.getPrivateKey()));
		user.setNameUser(decrypt(Base64.getDecoder().decode(user.getNameUser()), cryptographyModel.getPrivateKey()));
		repositoryUser.save(user);
		return ResponseEntity.noContent().build();
	}

	private CryptographyModel getCryptografyByUser(Long idUser) {
		return repositoryCrypt.findByIdUser(idUser)
				.orElseThrow(() -> new ApiException("Não existe chave de criptografia para este usuário."));
	}

	public static byte[] encrypt(String text, String publicKeyByUser) {
		try {
			Cipher cipher = Cipher.getInstance(METHOD);
			PublicKey publicKey = decodePublicKey(publicKeyByUser);
			cipher.init(Cipher.ENCRYPT_MODE, publicKey);
			return cipher.doFinal(text.getBytes());
		} catch (Exception e) {
			throw new ApiException("Erro ao Criptografar");
		}
	}

	private static PublicKey decodePublicKey(String publicKeyBase64) {
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKeyBase64.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance(METHOD);
			return keyFactory.generatePublic(keySpec);
		} catch (Exception e) {
			throw new ApiException("Erro Chave publica nao e valida.");
		}
	}

	public static String decrypt(byte[] text, String privateKeyByUser) {
		try {
			final Cipher cipher = Cipher.getInstance(METHOD);
			PrivateKey privateKey = decodePrivateKey(privateKeyByUser);
			cipher.init(Cipher.DECRYPT_MODE, privateKey);
			return new String(cipher.doFinal(text));
		} catch (Exception e) {
			throw new ApiException("Erro ao Descriptografar");
		}
	}

	private static PrivateKey decodePrivateKey(String privateKeyBase64) {
		try {
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
					Base64.getDecoder().decode(privateKeyBase64.getBytes()));
			KeyFactory keyFactory = KeyFactory.getInstance(METHOD);
			return keyFactory.generatePrivate(keySpec);
		} catch (Exception e) {
			throw new ApiException("Erro Chave privada nao e valida.");
		}

	}

}
