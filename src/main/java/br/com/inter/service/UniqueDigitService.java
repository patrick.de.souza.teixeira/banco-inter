package br.com.inter.service;

import org.springframework.stereotype.Service;

import br.com.inter.domain.dto.request.UniqueDigitDTORequest;
import br.com.inter.domain.dto.response.UniqueDigitDTOResponse;

@Service
public class UniqueDigitService {

	public UniqueDigitDTOResponse calcUniqueDigit(UniqueDigitDTORequest uniqueDigitDTORequest) {
		String valueConcat = uniqueDigitDTORequest.getDigit().repeat(uniqueDigitDTORequest.getMultiplier()).replace("0",
				"");
		while (valueConcat.length() > 1) {
			valueConcat = sumValue(valueConcat).toString();
		}
		var uniqueDigitDTOResponse = new UniqueDigitDTOResponse();

		uniqueDigitDTOResponse.setDigit(uniqueDigitDTORequest.getDigit());
		uniqueDigitDTOResponse.setMultiplier(uniqueDigitDTORequest.getMultiplier());
		uniqueDigitDTOResponse.setUniqueDigit(Integer.parseInt(valueConcat));

		return uniqueDigitDTOResponse;
	}

	private Integer sumValue(String valueConcat) {
		String[] numbers = valueConcat.split("");
		Integer total = 0;
		for (String number : numbers) {
			total = total + (Integer.valueOf(number));
		}
		return total;
	}

}
